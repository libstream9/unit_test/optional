#include <stream9/optional/optional.hpp>
#include <stream9/optional/reference.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace st9 = stream9;

using st9::optional;

BOOST_AUTO_TEST_SUITE(comparison_)

    BOOST_AUTO_TEST_CASE(equality_against_optional_)
    {
        optional<std::string> v1 = "foo";
        optional<std::string> v2 = "bar";
        optional<std::string> v3;

        BOOST_TEST(v1 == v1);
        BOOST_TEST(v1 != v2);
        BOOST_TEST(v1 != v3);
    }

    BOOST_AUTO_TEST_CASE(ordering_against_optional_)
    {
        optional<int> v1 = 1;
        optional<int> v2 = 2;
        optional<int> v3;

        BOOST_TEST(v1 < v2);
        BOOST_TEST(v2 > v1);
        BOOST_TEST(v1 > v3);
        BOOST_TEST(v3 < v1);
    }

    BOOST_AUTO_TEST_CASE(equality_against_value_)
    {
        optional<std::string> v1 = "foo";
        auto v2 = "bar";
        optional<std::string> v3;

        BOOST_TEST(v1 == "foo");
        BOOST_TEST(v1 != v2);
        BOOST_TEST(v1 != v3);
    }

    BOOST_AUTO_TEST_CASE(ordering_against_value_)
    {
        optional<int> v1 = 1;
        int v2 = 2;
        optional<int> v3;

        BOOST_TEST(v1 < v2);
        BOOST_TEST(v2 > v1);
        BOOST_TEST(v2 > v3);
        BOOST_TEST(v3 < v2);
    }

    BOOST_AUTO_TEST_CASE(equality_against_null_)
    {
        using st9::null;

        optional<std::string> v1 = "foo";
        optional<std::string> v2;

        BOOST_CHECK(v1 != null);
        BOOST_CHECK(v2 == null);
    }

    BOOST_AUTO_TEST_CASE(ordering_against_null_)
    {
        using st9::null;

        optional<int> v1 = 1;

        BOOST_CHECK(v1 > null);
        BOOST_CHECK(null < v1);
    }

BOOST_AUTO_TEST_SUITE_END() // comparison_

BOOST_AUTO_TEST_SUITE(transform_)

    BOOST_AUTO_TEST_CASE(has_value_1_)
    {
        optional<int> v1 = 1;

        auto f1 = [](int i) { return i + 1; };

        auto v2 = v1 | f1;

        BOOST_REQUIRE(v2.has_value());
        BOOST_TEST(v2.value());
    }

    BOOST_AUTO_TEST_CASE(has_value_2_)
    {
        int i = 1;
        optional<int&> v1 = i;

        auto f1 = [](int i) { return i + 1; };

        auto v2 = v1 | f1;

        BOOST_REQUIRE(v2.has_value());
        BOOST_TEST(v2.value());
    }

    BOOST_AUTO_TEST_CASE(null_1_)
    {
        optional<int> v1;

        auto f1 = [](int i) { return i + 1; };

        auto v2 = v1 | f1;

        BOOST_REQUIRE(!v2.has_value());
    }

    BOOST_AUTO_TEST_CASE(null_2_)
    {
        optional<int&> v1;

        auto f1 = [](int i) { return i + 1; };

        auto v2 = v1 | f1;

        BOOST_REQUIRE(!v2.has_value());
    }

BOOST_AUTO_TEST_SUITE_END() // transform_

BOOST_AUTO_TEST_CASE(const_optional_cast_)
{
    using namespace stream9;

    int i = 100;
    optional<int const&> o1 { i };

    auto o2 = const_optional_cast<int&>(o1);

    BOOST_TEST(o2.has_value());
    BOOST_TEST(*o2 == 100);
}

} // namespace testing
