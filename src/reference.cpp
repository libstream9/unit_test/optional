#include <stream9/optional/reference.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace st9 = stream9;

using st9::optional;

BOOST_AUTO_TEST_SUITE(lvalue_ref_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(default_)
        {
            optional<int&> v1;

            BOOST_TEST(!v1.has_value());
        }

        BOOST_AUTO_TEST_CASE(null_)
        {
            optional<int&> v1 { st9::null };

            BOOST_TEST(!v1.has_value());
        }

        BOOST_AUTO_TEST_CASE(value_1_)
        {
            int i = 100;
            optional<int&> v1 { i };

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == 100);
        }

        BOOST_AUTO_TEST_CASE(value_2_)
        {
            int i = 100;
            optional<int const&> v1 { i };

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == 100);
        }

        BOOST_AUTO_TEST_CASE(value_assignment_1_)
        {
            int i = 100;
            optional<int&> v1;

            v1 = i;

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == 100);
        }

        BOOST_AUTO_TEST_CASE(value_assignment_2_)
        {
            int i = 100;
            optional<int const&> v1;

            v1 = i;

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == 100);
        }

        BOOST_AUTO_TEST_CASE(conversion_1_)
        {
            struct X {};
            struct Y : X {};

            Y i;
            optional<Y&> v1 { i };
            optional<X&> v2 { v1 };

            BOOST_REQUIRE(v2.has_value());
        }

        BOOST_AUTO_TEST_CASE(conversion_assignment_1_)
        {
            struct X {};
            struct Y : X {};

            Y i;
            optional<Y&> v1 { i };
            optional<X&> v2;

            v2 = v1;

            BOOST_REQUIRE(v2.has_value());
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_CASE(operators_)
    {
        std::string s = "foo";
        optional<std::string&> v1 = s;

        BOOST_REQUIRE(!!v1);
        BOOST_TEST(*v1 == "foo");
        BOOST_TEST(v1->size() == 3);
    }

    BOOST_AUTO_TEST_CASE(reset_)
    {
        int i = 100;
        optional<int&> v1 = i;

        v1.reset();

        BOOST_REQUIRE(!v1.has_value());
    }

    BOOST_AUTO_TEST_CASE(conversion_1_)
    {
        static_assert(std::is_convertible_v<optional<int&>, optional<int const&>>);
    }

BOOST_AUTO_TEST_SUITE_END() // lvalue_ref_

} // namespace testing
