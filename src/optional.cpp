#include <stream9/optional.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

#include <stream9/null.hpp>

namespace testing {

namespace st9 = stream9;

using st9::optional;
using st9::opt;

BOOST_AUTO_TEST_SUITE(optional_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(default_)
        {
            using T = optional<std::string>;
            static_assert(std::is_default_constructible_v<T>);
            static_assert(std::is_nothrow_default_constructible_v<T>);

            optional<std::string> v;

            BOOST_TEST(!v.has_value());
        }

        BOOST_AUTO_TEST_CASE(null_)
        {
            using T = optional<std::string>;
            static_assert(std::is_constructible_v<T, st9::null_t>);
            static_assert(std::is_nothrow_constructible_v<T, st9::null_t>);

            optional<std::string> v { st9::null };

            BOOST_TEST(!v.has_value());
        }

        BOOST_AUTO_TEST_CASE(copy_)
        {
            using T = optional<std::string>;
            static_assert(std::is_copy_constructible_v<T>);
            static_assert(!std::is_nothrow_copy_constructible_v<T>);

            T v1 { "foo" };

            T v2 { v1 };

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == "foo");
            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(copy_assignment_)
        {
            using T = optional<std::string>;
            static_assert(std::is_copy_assignable_v<T>);
            static_assert(!std::is_nothrow_copy_assignable_v<T>);

            T v1 { "foo" };

            T v2 = v1;

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == "foo");
            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(move_)
        {
            using T = optional<std::string>;
            static_assert(std::is_move_constructible_v<T>);
            static_assert(std::is_nothrow_move_constructible_v<T>);

            T v1 { "foo" };

            T v2 { std::move(v1) };

            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(move_assignment_)
        {
            using T = optional<std::string>;
            static_assert(std::is_move_assignable_v<T>);
            static_assert(std::is_nothrow_move_assignable_v<T>);

            T v1 { "foo" };

            T v2 = std::move(v1);

            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(conversion_copy_)
        {
            using T1 = optional<char const*>;
            using T2 = optional<std::string>;
            static_assert(std::is_constructible_v<T2, T1>);
            static_assert(!std::is_nothrow_constructible_v<T2, T1>);

            T1 v1 { "foo" };

            T2 v2 { v1 };

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == "foo");
            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(conversion_move_)
        {
            using T1 = optional<char const*>;
            using T2 = optional<std::string>;
            static_assert(std::is_constructible_v<T2, T1>);
            static_assert(!std::is_nothrow_constructible_v<T2, T1>);

            T1 v1 { "foo" };

            T2 v2 { std::move(v1) };

            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(conversion_copy_assignment_1_)
        {
            using T1 = optional<char const*>;
            using T2 = optional<std::string>;
            static_assert(std::is_constructible_v<T2, T1>);
            static_assert(!std::is_nothrow_constructible_v<T2, T1>);

            T1 v1 { "foo" };

            T2 v2;
            v2 = v1;

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == "foo");
            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(conversion_copy_assignment_2_)
        {
            using T1 = optional<char const*>;
            using T2 = optional<std::string>;
            static_assert(std::is_constructible_v<T2, T1>);
            static_assert(!std::is_nothrow_constructible_v<T2, T1>);

            T1 v1 { "foo" };

            T2 v2 { "bar" };
            v2 = v1;

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == "foo");
            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(conversion_move_assignment_1_)
        {
            using T1 = optional<char const*>;
            using T2 = optional<std::string>;
            static_assert(std::is_constructible_v<T2, T1>);
            static_assert(!std::is_nothrow_constructible_v<T2, T1>);

            T1 v1 { "foo" };

            T2 v2;
            v2 = std::move(v1);

            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(conversion_move_assignment_2_)
        {
            using T1 = optional<char const*>;
            using T2 = optional<std::string>;
            static_assert(std::is_constructible_v<T2, T1>);
            static_assert(!std::is_nothrow_constructible_v<T2, T1>);

            T1 v1 { "foo" };

            T2 v2 { "bar" };
            v2 = std::move(v1);

            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(conversion_from_reference_)
        {
            using T1 = optional<std::string const&>;
            using T2 = optional<std::string_view>;
            static_assert(std::is_constructible_v<T2, T1>);
            static_assert(std::is_nothrow_constructible_v<T2, T1>);

            std::string s1 = "foo";

            T1 v1 { s1 };
            T2 v2 { v1 };

            T2 v3 = v1;
            T2 v4;
            v4 = v1;

            BOOST_REQUIRE(v1.has_value());
            BOOST_TEST(v1.value() == s1);
            BOOST_REQUIRE(v2.has_value());
            BOOST_TEST(v2.value() == s1);
            BOOST_REQUIRE(v3.has_value());
            BOOST_TEST(v3.value() == s1);
            BOOST_REQUIRE(v4.has_value());
            BOOST_TEST(v4.value() == s1);
        }

        BOOST_AUTO_TEST_CASE(value_copy_)
        {
            using T = optional<std::string>;
            static_assert(std::is_constructible_v<T, std::string&>);
            static_assert(!std::is_nothrow_constructible_v<T, std::string&>);

            std::string s = "foo";

            T v { s };

            BOOST_REQUIRE(v.has_value());
            BOOST_TEST(v.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(value_move_)
        {
            using T = optional<std::string>;
            static_assert(std::is_constructible_v<T, std::string&&>);
            static_assert(std::is_nothrow_constructible_v<T, std::string&&>);

            std::string s = "foo";

            T v { std::move(s) };

            BOOST_REQUIRE(v.has_value());
            BOOST_TEST(v.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(value_copy_assign_1_)
        {
            using T = optional<std::string>;

            std::string s = "foo";

            T v;
            v = s;

            BOOST_REQUIRE(v.has_value());
            BOOST_TEST(v.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(value_copy_assign_2_)
        {
            using T = optional<std::string>;

            std::string s = "foo";

            T v = "bar";
            v = s;

            BOOST_REQUIRE(v.has_value());
            BOOST_TEST(v.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(value_move_assign_1_)
        {
            using T = optional<std::string>;

            std::string s = "foo";

            T v;
            v = std::move(s);

            BOOST_REQUIRE(v.has_value());
            BOOST_TEST(v.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(value_move_assign_2_)
        {
            using T = optional<std::string>;

            std::string s = "foo";

            T v = "bar";
            v = std::move(s);

            BOOST_REQUIRE(v.has_value());
            BOOST_TEST(v.value() == "foo");
        }

        BOOST_AUTO_TEST_CASE(value_emplace_)
        {
            optional<std::string> v { "foo" };

            BOOST_REQUIRE(v.has_value());
            BOOST_TEST(v.value() == "foo");
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_CASE(emplace_)
    {
        optional<std::string> v1;

        v1.emplace("foo");

        BOOST_REQUIRE(v1.has_value());
        BOOST_TEST(v1.value() == "foo");
    }

    BOOST_AUTO_TEST_CASE(reset_)
    {
        optional<std::string> v1 { "foo" };

        v1.reset();

        BOOST_REQUIRE(!v1.has_value());
    }

    BOOST_AUTO_TEST_CASE(swap_1_)
    {
        optional<std::string> v1;
        optional<std::string> v2;

        v1.swap(v2);

        BOOST_REQUIRE(!v1.has_value());
        BOOST_REQUIRE(!v2.has_value());
    }

    BOOST_AUTO_TEST_CASE(swap_2_)
    {
        optional<std::string> v1 { "foo" };
        optional<std::string> v2;

        v1.swap(v2);

        BOOST_REQUIRE(!v1.has_value());
        BOOST_REQUIRE(v2.has_value());
    }

    BOOST_AUTO_TEST_CASE(swap_3_)
    {
        optional<std::string> v1;
        optional<std::string> v2 { "foo" };

        v1.swap(v2);

        BOOST_REQUIRE(v1.has_value());
        BOOST_REQUIRE(!v2.has_value());
    }

    BOOST_AUTO_TEST_CASE(swap_4_)
    {
        optional<std::string> v1 { "foo" };
        optional<std::string> v2 { "bar" };

        v1.swap(v2);

        BOOST_REQUIRE(v1.has_value());
        BOOST_TEST(v1.value() == "bar");
        BOOST_REQUIRE(v2.has_value());
        BOOST_TEST(v2.value() == "foo");
    }

    BOOST_AUTO_TEST_CASE(value_)
    {
        opt<int> v1;
        opt<int> const v2;

        static_assert(std::same_as<decltype(v1.value()), int&>);
        static_assert(std::same_as<decltype(v2.value()), int const&>);
        static_assert(std::same_as<decltype(std::move(v1).value()), int&&>);
        static_assert(std::same_as<decltype(opt<int>().value()), int&&>);
    }

    BOOST_AUTO_TEST_CASE(or_throw_)
    {
        opt<int> v1;
        opt<int> const v2;
        auto fn = []{ return 1; };

        static_assert(std::same_as<decltype(v1.or_throw(fn)), int&>);
        static_assert(std::same_as<decltype(v2.or_throw(fn)), int const&>);
        static_assert(std::same_as<decltype(std::move(v1).or_throw(fn)), int&&>);
        static_assert(std::same_as<decltype(opt<int>().or_throw(fn)), int&&>);

        BOOST_CHECK_THROW(v1.or_throw(fn), int);
    }

BOOST_AUTO_TEST_SUITE_END() // optional_

BOOST_AUTO_TEST_SUITE(nullable_)

    class positive_int
    {
    public:
        positive_int() = default;

        auto value() const { return m_v; }

    private:
        positive_int(stream9::null_t)
            : m_v { -1 }
        {}

        friend struct stream9::null_traits<positive_int>;

        bool is_null() const { return m_v == -1; }
        void set_null() { m_v = -1; }

    private:
        int m_v = 0;
    };

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        static_assert(stream9::nullable<positive_int>);

        using T = optional<positive_int>;

        optional<positive_int> a;

        static_assert(sizeof(a) == sizeof(int));
        static_assert(std::is_trivially_copy_constructible_v<T>);
        static_assert(std::is_trivially_copy_assignable_v<T>);
        static_assert(std::is_trivially_move_constructible_v<T>);
        static_assert(std::is_trivially_move_assignable_v<T>);
        static_assert(std::is_trivially_destructible_v<T>);

        BOOST_TEST(!a.has_value());

        a.emplace();

        BOOST_REQUIRE(a.has_value());
        BOOST_CHECK((*a).value() == 0);

        a.reset();
        BOOST_TEST(!a.has_value());
    }

BOOST_AUTO_TEST_SUITE_END() // nullable_

BOOST_AUTO_TEST_SUITE(transitive_arrow_)

    struct foo {
        static constexpr bool transitive_arrow = true;

        int operator->() const
        {
            return 100;
        }
    };

    BOOST_AUTO_TEST_CASE(step_1_)
    {
        optional<foo> o;

        static_assert(std::same_as<decltype(o.operator->()), int>);
    }

    BOOST_AUTO_TEST_CASE(step_2_)
    {
        optional<optional<optional<optional<int>>>> o;

        using T = decltype(o.operator->());

        static_assert(std::same_as<T, int*>);
    }

BOOST_AUTO_TEST_SUITE_END() // transitive_arrow_

} // namespace testing
