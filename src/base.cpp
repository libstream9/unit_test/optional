#include <stream9/optional/base.hpp>
#include <stream9/optional/optional.hpp>

#include <string>
#include <optional>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace stream9::optional_;

template<typename T>
using opt = stream9::optional<T>;

BOOST_AUTO_TEST_CASE(trivial_)
{
    using T = opt<int>;

    static_assert(std::is_default_constructible_v<T>);
    static_assert(std::is_trivially_destructible_v<T>);
    static_assert(std::is_trivially_copy_constructible_v<T>);
    static_assert(std::is_trivially_move_constructible_v<T>);
    static_assert(std::is_trivially_copy_assignable_v<T>);
    static_assert(std::is_trivially_move_assignable_v<T>);
}

BOOST_AUTO_TEST_CASE(trivial_2_)
{
    using U = stream9::optional_::storage<std::string>;
    U u;
    static_assert(std::is_default_constructible_v<U>);

    using T = opt<std::string>;

    static_assert(std::is_default_constructible_v<T>);
    static_assert(!std::is_trivially_destructible_v<T>);
    static_assert(!std::is_trivially_copy_constructible_v<T>);
    static_assert(!std::is_trivially_move_constructible_v<T>);
    static_assert(!std::is_trivially_copy_assignable_v<T>);
    static_assert(!std::is_trivially_move_assignable_v<T>);
}

BOOST_AUTO_TEST_CASE(trivial_3_)
{
    struct foo
    {
        foo(foo const&) = delete;
        foo& operator=(foo const&) = delete;
        foo(foo&&) = default;
        foo& operator=(foo&&) = default;
    };

    using T = opt<foo>;

    static_assert(!std::is_copy_constructible_v<T>);
    static_assert(!std::is_copy_assignable_v<T>);
    static_assert(std::is_move_constructible_v<T>);
    static_assert(std::is_move_assignable_v<T>);
}

} // namespace testing
